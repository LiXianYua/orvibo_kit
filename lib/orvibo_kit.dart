import 'dart:async';
import 'dart:core';
import 'dart:io';
import 'dart:isolate';

import 'package:date_format/date_format.dart';
import 'package:orvibo_kit/engine/orvibo_kit_binding.dart';
import 'package:orvibo_kit/kit/apm/leak_detector/leak_detector.dart';
import 'package:orvibo_kit/kit/apm/log_kit.dart';
import 'package:orvibo_kit/ui/orvibo_kit_app.dart';
import 'package:orvibo_kit/ui/orvibo_kit_btn.dart';
import 'package:orvibo_kit/ui/kit_page.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart' as dart;
import 'package:file/local.dart';
import 'package:file/file.dart';


export 'package:orvibo_kit/ui/orvibo_kit_app.dart';

typedef OrviboKitAppCreator = Future<IOrviboKitApp> Function();
typedef LogCallback = void Function(String);
typedef ExceptionCallback = void Function(dynamic, StackTrace);

const String OK_PACKAGE_NAME = 'orvibo_kit';

//默认release模式不开启该功能
const bool release = kReleaseMode;

//记录当前zone
Zone? _zone;

// ignore: avoid_classes_with_only_static_members
class OrviboKit {
  // 初始化方法,app或者appCreator必须设置一个
  static Future<void> runApp(
      {OrviboKitApp? app,
      OrviboKitAppCreator? appCreator,
      bool isEnable = true,
      bool useInRelease = false,
      LogCallback? logCallback,
      ExceptionCallback? exceptionCallback,
      List<String> methodChannelBlackList = const <String>[],
      Function? releaseAction}) async {

    assert(app != null || appCreator != null, 'app and appCreator are both null');

    await _initLogFile();
    FlutterError.onError = (FlutterErrorDetails details) async {
      _collectError(details.exception, details.stack);
    };
    Isolate.current.addErrorListener(RawReceivePort((dynamic pair) async {
      final isolateError = pair as List<dynamic>;
      _collectError(
        isolateError.first.toString(),
        isolateError.last.toString(),
      );
    }).sendPort);
    await runZonedGuarded(
      () async {
        if (release && !useInRelease && !isEnable) {
          if (releaseAction != null) {
            releaseAction.call();
          } else {
            dart.runApp((app ?? await appCreator!()).origin);
          }
          return;
        }else{
          blackList = methodChannelBlackList;
          LeakDetector().init();
          _ensureOrviboKitBinding(useInRelease: useInRelease);
          _runWrapperApp(app ?? await appCreator!());
        }
        _zone = Zone.current;
      },
      (Object obj, StackTrace stack) {
        _collectError(obj, stack);
        if (exceptionCallback != null) {
          _zone?.runBinary(exceptionCallback, obj, stack);
        }
      },
      zoneSpecification: ZoneSpecification(print: (Zone self, ZoneDelegate parent, Zone zone, String line) {
          _collectLog(line); //收集日志
          parent.print(zone, line);
          if (logCallback != null) {
            _zone?.runUnary(logCallback, line);
          }
        },
      ),
    );
  }
}

// 如果在runApp之前执行了WidgetsFlutterBinding.ensureInitialized，会导致methodchannel功能不可用，可以在runApp前先调用一下ensureOrviboKitBinding
void _ensureOrviboKitBinding({bool useInRelease = false}) {
  if (!release || useInRelease) {
    OrviboKitWidgetsFlutterBinding.ensureInitialized();
  }
}

void _runWrapperApp(IOrviboKitApp wrapper) {
  OrviboKitWidgetsFlutterBinding.ensureInitialized()
// ignore: invalid_use_of_protected_member
    ?..scheduleAttachRootWidget(wrapper)
    ..scheduleWarmUpFrame();
  addEntrance();
}

void _collectLog(String line) {
  LogManager.instance.addLog(LogBean.TYPE_INFO, line);
}

void _collectError(Object? details, Object? stack) async {
  if(_isErrorToFile){
    await _checkDeleteLog();
    var logFile = await _getErrorLogFile();
    logFile?.writeAsString(details.toString()+"\n"+stack.toString());
  }
  LogManager.instance.addLog(
        LogBean.TYPE_ERROR, '${details?.toString()}\n${stack?.toString()}');

}

Directory? _cacheDir;
bool _isErrorToFile = false;
Future<void> _initLogFile() async {
  try{
    var cacheDir = const LocalFileSystem().directory("/data/log/mixpad_gui");
    var isCacheDirExists = await cacheDir.exists();
    if(!isCacheDirExists){
      isCacheDirExists = await (await cacheDir.create(recursive: true)).exists();
      if(!isCacheDirExists){
        print("无法创建错误日志目录");
        _isErrorToFile = false;
        return;
      }
      print("创建错误日志目录成功");
    }
    _cacheDir = cacheDir;
    _isErrorToFile = true;
  }catch(e,s){
    _isErrorToFile = false;
    print(e);
    print(s);
  }
}

const _errorLogDateFormat = [yyyy,mm,dd,HH,nn,ss];
Future<File?> _getErrorLogFile() async{
  var now = DateTime.now();
  var logFile = _cacheDir?.childFile("crash_${formatDate(now, _errorLogDateFormat)}${OrviboKitApp.version!=null?"_"+OrviboKitApp.version.toString():""}.log");
  if(await logFile?.exists() == true){
    logFile?.delete();
  }
  return logFile?.create(recursive: true);
}

Future<void> _checkDeleteLog() async {
  var cacheDir = _cacheDir;
  if(cacheDir==null) return;
  var logFiles = cacheDir.listSync().where((element) => element.basename.endsWith(".log")).toList();
  if(logFiles.length>10){
    for (var value in logFiles.take(logFiles.length - 10)) {
      value.delete();
    }
  }
}

void addEntrance() {
  WidgetsBinding.instance?.addPostFrameCallback((_) {
    final floatBtn = OrviboKitBtn();
    floatBtn.addToOverlay();
    KitPageManager.instance.loadCache();
  });
}

void dispose({required BuildContext context}) {
  orviboKitOverlayKey.currentState?.widget.initialEntries.forEach((element) {
// element.remove();
  });
}
