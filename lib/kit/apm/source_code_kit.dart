import 'dart:convert';

import 'package:orvibo_kit/orvibo_kit.dart';
import 'package:orvibo_kit/kit/apm/apm.dart';
import 'package:orvibo_kit/kit/apm/vm/vm_helper.dart';
import 'package:orvibo_kit/kit/kit.dart';
import 'package:orvibo_kit/widget/source_code/source_code_view.dart';
import 'package:flutter/material.dart';

class SourceCodeKit extends ApmKit {
  @override
  Widget createDisplayPage() {
    return const SourceCodePage();
  }

  @override
  IStorage createStorage() {
    return CommonStorage(maxCount: 120);
  }

  @override
  String getIcon() {
    return 'images/ok_source_code.png';
  }

  @override
  String getKitName() {
    return ApmKitName.KIT_SOURCE_CODE;
  }

  @override
  void start() {}

  @override
  void stop() {}
}

class SourceCodePage extends StatefulWidget {

  const SourceCodePage({Key? key}):super(key: key);

  @override
  _SourceCodePageState createState() => _SourceCodePageState();
}

class _SourceCodePageState extends State<SourceCodePage> {
  String? sourceCode;

  static const String _orvibokitSourceCodeGroup = 'orvibokit_source_code-group';

  @override
  void dispose() {
    // ignore: invalid_use_of_protected_member
    WidgetInspectorService.instance.disposeGroup(_orvibokitSourceCodeGroup);
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      var renderObject = findTopRenderObject();
      if (renderObject == null) {
        return;
      }
      WidgetInspectorService.instance.selection.current = renderObject;
      final id = WidgetInspectorService.instance
          // ignore: invalid_use_of_protected_member
          .toId(renderObject.toDiagnosticsNode(), _orvibokitSourceCodeGroup);
      if (id == null) {
        return;
      }
      final String? nodeDesc = WidgetInspectorService.instance
          .getSelectedSummaryWidget(id, _orvibokitSourceCodeGroup);
      if (nodeDesc != null) {
        final Map<String, dynamic>? map =
            json.decode(nodeDesc) as Map<String, dynamic>?;
        if (map != null) {
          final Map<String, dynamic>? location =
              map['creationLocation'] as Map<String, dynamic>?;
          if (location != null) {
            final fileLocation = location['file'] as String;
            final fileName = fileLocation.split("/").last;
            getScriptList(fileName).then((value) => {
                  setState(() {
                    sourceCode = value!.source;
                  })
                });
          }
        }
      }
    });
  }

  RenderObject? findTopRenderObject() {
    Element? topElement;
    var context = OrviboKitApp.appKey.currentContext;
    if (context == null) {
      return null;
    }
    final ModalRoute<dynamic>? rootRoute = ModalRoute.of(context);
    void listTopView(Element element) {
      if (element.widget is! PositionedDirectional) {
        if (element is RenderObjectElement &&
            element.renderObject is RenderBox) {
          final ModalRoute<dynamic>? route = ModalRoute.of(element);
          if (route != null && route != rootRoute) {
            topElement = element;
          }
        }
        element.visitChildren(listTopView);
      }
    }

    context.visitChildElements(listTopView);
    if (topElement != null) {
      return topElement!.renderObject;
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints.expand(),
      child: sourceCode == null
          ? const Center(
              child: Text("加载中"),
            )
          : Padding(
              padding: const EdgeInsets.all(8.0),
              child: SourceCodeView(sourceCode: sourceCode ?? ''),
            ),
    );
  }
}
