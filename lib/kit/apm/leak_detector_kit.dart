import 'dart:async';
import 'dart:ui';

import 'package:orvibo_kit/kit/apm/leak_detector/leak_data.dart';
import 'package:orvibo_kit/kit/apm/leak_detector/leak_detector.dart';
import 'package:orvibo_kit/kit/apm/apm.dart';
import 'package:orvibo_kit/kit/apm/vm/vm_service_wrapper.dart';
import 'package:orvibo_kit/kit/kit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'leak_detector/view/leak_preview_page.dart';

class LeakDetectorKit extends ApmKit {
  final List<LeakedInfo> _leakedList = [];
  StreamSubscription<LeakedInfo>? _subscription;
  LeakedInfo? currentLeakedInfo;

  @override
  Widget? getDetailPage() {
    var info = currentLeakedInfo;
    if(info!=null){
      return LeakedInfoWidget(leakInfo: info,);
    }else{
      return null;
    }
  }

  @override
  String getKitName() {
    return ApmKitName.KIT_LEAK_DETECTOR;
  }

  @override
  String getIcon() {
    return 'images/ok_ram.png';
  }

  @override
  void start() {
    LeakDetector().init();
    _subscription = LeakDetector().onLeakedStream.listen((event) {
      _leakedList.add(event);
    });
  }

  @override
  void stop() {
    _subscription?.cancel();
    VMServiceWrapper.instance.disConnect();
  }

  @override
  IStorage createStorage() {
    return CommonStorage(maxCount: 120);
  }

  @override
  Widget createDisplayPage() {
    return const LeakDetectorPage();
  }

  List<LeakedInfo> getAllLeakedInfo() {
    return _leakedList;
  }
}

class LeakDetectorPage extends StatefulWidget {

  const LeakDetectorPage({Key? key}):super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LeakDetectorPageState();
  }
}

class LeakDetectorPageState extends State<LeakDetectorPage> {
  LeakDetectorKit? kit = ApmKitManager.instance.getKit<LeakDetectorKit>(ApmKitName.KIT_LEAK_DETECTOR);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.all(16),
        child: StreamBuilder(builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          return ListView.builder(
              padding: const EdgeInsets.all(0),
              itemCount: kit?.getAllLeakedInfo().length??0,
              itemBuilder: (context, index) {
                return LeakedInfoItemWidget(
                  item: kit!.getAllLeakedInfo()[index],
                  index: index,
                );
              });
        },
          stream: LeakDetector().onLeakedStream,
        ));
  }
}

class LeakedInfoItemWidget extends StatelessWidget {
  final LeakedInfo item;
  final int index;

  const LeakedInfoItemWidget({Key? key, required this.item, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final showDate = DateTime.fromMillisecondsSinceEpoch(item.timestamp!);
    return InkWell(child: Container(
      height: 40,
      color: index % 2 == 1 ? const Color(0xfffafafa) : Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            width: 80,
            alignment: Alignment.center,
            child: Text(item.retainingPath.first.clazz,
                style: const TextStyle(color: Color(0xff333333), fontSize: 12)),
          ),
          Container(
            width: 80,
            alignment: Alignment.center,
            child: Text(item.gcRootType??"",
                style: const TextStyle(color: Color(0xff333333), fontSize: 12)),
          ),
          Container(
              width: MediaQuery.of(context).size.width - 193,
              alignment: Alignment.center,
              child: Text('${showDate.month}/${showDate.day} ${showDate.hour}:${showDate.minute}:${showDate.second}',
                  style: const TextStyle(color: Color(0xff333333), fontSize: 12))),
        ],
      ),
    ),onTap: (){
      ApmKitManager.instance.getKit<LeakDetectorKit>(ApmKitName.KIT_LEAK_DETECTOR)?.currentLeakedInfo = item;
      ApmKitManager.instance.getKit<LeakDetectorKit>(ApmKitName.KIT_LEAK_DETECTOR)?.tabAction();
      //showLeakedInfoPage(context,item);
    },);
  }
}
