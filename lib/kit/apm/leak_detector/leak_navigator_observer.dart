import 'package:flutter/widgets.dart';

import 'leak_detector.dart';

///delay check leak
///有时候有些页面会使用延迟回调函数，比如WebSocket就是delay close connect。
const int _defaultCheckLeakDelay = 500;

typedef ShouldAddedRoute = bool Function(Route route);

///NavigatorObserver
class LeakNavigatorObserver extends NavigatorObserver {
  final ShouldAddedRoute? shouldCheck;
  final int checkLeakDelay;

  ///[callback] 如果为null则检查所有路由否则只在返回true时检查.
  LeakNavigatorObserver(
      {this.checkLeakDelay = _defaultCheckLeakDelay, this.shouldCheck});

  @override
  void didPop(Route route, Route? previousRoute) {
    _remove(route);
  }

  @override
  void didPush(Route route, Route? previousRoute) {
    _add(route);
  }

  @override
  void didRemove(Route route, Route? previousRoute) {
    _remove(route);
  }

  @override
  void didReplace({Route? newRoute, Route? oldRoute}) {
    if (newRoute != null) {
      _add(newRoute);
    }
    if (oldRoute != null) {
      _remove(oldRoute);
    }
  }

  ///add a object to LeakDetector
  void _add(Route route) {
    debugPrint("添加检查："+route.toString());
    assert(() {
      if (route is ModalRoute &&
          (shouldCheck == null || shouldCheck!.call(route))) {
        route.didPush().then((_) {
          final element = _getElementByRoute(route);
          if (element != null) {

            final key = _getRouteKey(route);
            watchObjectLeak(element, key); //Element
            watchObjectLeak(element.widget, key); //Widget
            if (element is StatefulElement) {
              watchObjectLeak(element.state, key); //State
            }
          }
        });
      }

      return true;
    }());
  }

  ///check and analyze the route
  void _remove(Route route) {
    assert(() {
      final element = _getElementByRoute(route);
      if (element != null) {
        final key = _getRouteKey(route);
        if (element is StatefulElement || element is StatelessElement) {
          debugPrint("开始检查："+key);
          //start check
          LeakDetector().ensureReleaseAsync(key, delay: _defaultCheckLeakDelay);
        }
      }

      return true;
    }());
  }

  ///add obj into the group
  watchObjectLeak(Object obj, String name) {
    assert(() {
      LeakDetector().addWatchObject(obj, name);
      return true;
    }());
  }

  ///Get the ‘Element’ of our custom page
  Element? _getElementByRoute(Route route) {
    Element? element;
    if (route is ModalRoute &&
        (shouldCheck == null || shouldCheck!.call(route))) {
      //RepaintBoundary
      route.subtreeContext?.visitChildElements((child) {
        //Builder
        child.visitChildElements((child) {
          //Semantics
          child.visitChildElements((child) {
            //My Page
            element = child;
          });
        });
      });
    }
    return element;
  }

  ///generate key by [Route]
  String _getRouteKey(Route route) {
    final hasCode = route.hashCode.toString();
    String? key = route.settings.name;
    if (key == null || key.isEmpty) {
      key = route.hashCode.toString();
    } else {
      key = '$key($hasCode)';
    }
    return key;
  }
}
