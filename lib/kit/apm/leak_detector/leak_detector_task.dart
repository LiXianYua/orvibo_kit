import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:vm_service/vm_service.dart';

import 'leak_detector.dart';
import 'leak_analyzer.dart';
import 'leak_data.dart';
import 'vm_service_utils.dart';

///检查泄漏任务
abstract class _Task<T> {
  void start() async {
    T? result;
    try {
      result = await run();
    } catch (e) {
      print('_Task $e');
    } finally {
      done(result);
    }
  }

  Future<T?> run();

  ///确保在运行后调用
  void done(T? result);
}

class DetectorTask extends _Task {
  Expando? expando;

  final VoidCallback? onStart;
  final Function(dynamic result)? onResult;
  final Function(LeakedInfo? leakInfo)? onLeaked;
  final StreamSink<DetectorEvent>? sink;

  DetectorTask(
    this.expando, {
    required this.onResult,
    required this.onLeaked,
    this.onStart,
    this.sink,
  });

  @override
  void done(Object? result) {
    onResult?.call(result);
  }

  @override
  Future<LeakedInfo?> run() async {
    if (expando != null) {
      onStart?.call();
      if (await _maybeHasLeaked()) {
        //运行 GC，确保对象应该释放
        sink?.add(DetectorEvent(DetectorEventType.startGC));
        await VmServerUtils().startGCAsync(); //GC
        sink?.add(DetectorEvent(DetectorEventType.endGc));
        return await _analyzeLeakedPathAfterGC();
      }
    }
    return null;
  }

  ///Full GC后，检查是否有泄漏，如果有泄漏分析引用链
  Future<LeakedInfo?> _analyzeLeakedPathAfterGC() async {
    List<dynamic> weakPropertyList =
        await _getExpandoWeakPropertyList(expando!);
    expando = null; //一定要释放引用
    for (var weakProperty in weakPropertyList) {
      if (weakProperty != null) {
        final leakedInstance = await _getWeakPropertyKey(weakProperty.id);
        if (leakedInstance != null) {
          final start = DateTime.now();
          sink?.add(DetectorEvent(DetectorEventType.startAnalyze));
          LeakedInfo? leakInfo = await compute(
            LeakAnalyzer.analyze,
            AnalyzeData(leakedInstance, LeakDetector.maxRetainingPath),
            debugLabel: 'analyze',
          );
          sink?.add(DetectorEvent(DetectorEventType.endAnalyze,
              data: DateTime.now().difference(start)));
          onLeaked?.call(leakInfo);
        }
      }
    }
    return null;
  }

  ///some weak reference != null;
  Future<bool> _maybeHasLeaked() async {
    List<dynamic> weakPropertyList =
        await _getExpandoWeakPropertyList(expando!);
    for (var weakProperty in weakPropertyList) {
      if (weakProperty != null) {
        final leakedInstance = await _getWeakPropertyKey(weakProperty.id);
        if (leakedInstance != null) return true;
      }
    }
    return false;
  }

  ///List Item has id
  Future<List<dynamic>> _getExpandoWeakPropertyList(Expando expando) async {
    if (await VmServerUtils().hasVmService) {
      final data = (await VmServerUtils().getInstanceByObject(expando))
          ?.getFieldValueInstance('_data');
      if (data?.id != null) {
        final dataObj = await VmServerUtils().getObjectInstanceById(data.id);
        if (dataObj?.json != null) {
          Instance? weakListInstance = Instance.parse(dataObj!.json!);
          if (weakListInstance != null) {
            return weakListInstance.elements ?? [];
          }
        }
      }
    }
    return [];
  }

  ///在 [Expando] 中获取 PropertyKey
  Future<InstanceRef?> _getWeakPropertyKey(String weakPropertyId) async {
    final weakPropertyObj =
        await VmServerUtils().getObjectInstanceById(weakPropertyId);
    if (weakPropertyObj != null) {
      final weakPropertyInstance = Instance.parse(weakPropertyObj.json);
      return weakPropertyInstance?.propertyKey;
    }
    return null;
  }
}
