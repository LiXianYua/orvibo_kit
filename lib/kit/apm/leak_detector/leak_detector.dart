import 'dart:async';
import 'dart:collection';
import 'dart:ffi';

import 'package:flutter/widgets.dart';

import 'leak_detector_task.dart';
import 'leak_data.dart';
import 'leak_record_handler.dart';
import 'vm_service_utils.dart';

typedef LeakEventListener = void Function(DetectorEvent event);

///泄漏检测主要工具类
class LeakDetector {
  static LeakDetector? _instance;

  ///[VmService.getRetainingPath]limit
  static int? maxRetainingPath;

  ///检测的 object
  final Map<String, Expando> _watchGroup = {};

  ///检测内存泄漏队列，先进先出
  final Queue<DetectorTask> _checkTaskQueue = Queue();

  ///内存泄漏后通知
  final StreamController<LeakedInfo> _onLeakedStreamController = StreamController.broadcast();
  final StreamController<DetectorEvent> _onEventStreamController = StreamController.broadcast();

  DetectorTask? _currentTask;

  Stream<LeakedInfo> get onLeakedStream => _onLeakedStreamController.stream;

  Stream<DetectorEvent> get onEventStream => _onEventStreamController.stream;

  factory LeakDetector() {
    _instance ??= LeakDetector._();
    return _instance!;
  }

  void init({int maxRetainingPath = 300}) {
    LeakDetector.maxRetainingPath = maxRetainingPath;
  }

  LeakDetector._() {
    assert(() {
      VmServerUtils().getVmService(); //连接 VmService
      //onLeakedStream.listen(saveLeakedRecord); //添加监听器，保存泄露记录
      return true;
    }());
  }

  ///开始检测是否有内存泄漏
  ensureReleaseAsync(String? group, {int delay = 0}) async {
    print("expando:"+_watchGroup.toString());
    Expando? expando = _watchGroup[group];
    _watchGroup.remove(group);
    if (expando != null) {
      //延时检测，有些state会在页面退出之后延迟释放，这并不表示就一定是内存泄漏。
      //比如runZone就会延时释放
      debugPrint("延迟添加检查任务："+group.toString());
      Timer(Duration(milliseconds: delay), () async {
        // 添加检查任务
        _checkTaskQueue.add(
          DetectorTask(
            expando,
            sink: _onEventStreamController.sink,
            onStart: () => _onEventStreamController
                .add(DetectorEvent(DetectorEventType.check, data: group)),
            onResult: (result) {
              debugPrint("检查结果："+result.toString());
              _currentTask = null;
              _checkStartTask();
            },
            onLeaked: (LeakedInfo? leakInfo) {
              debugPrint("检查到泄漏："+leakInfo.toString());
              //通知监听
              if (leakInfo != null && leakInfo.isNotEmpty) {
                _onLeakedStreamController.add(leakInfo);
              }
            },
          ),
        );
        expando = null;
        _checkStartTask();
      });
    }
  }

  ///如果不为空则开始检查任务
  void _checkStartTask() {
    if (_checkTaskQueue.isNotEmpty && _currentTask == null) {
      _currentTask = _checkTaskQueue.removeFirst();
      _currentTask?.start();
    }
  }

  ///[group] 认为可以在一块释放的对象组，一般在一个[State]中想监听的对象
  addWatchObject(Object obj, String group) {
    if (LeakDetector.maxRetainingPath == null) return;

    _onEventStreamController
        .add(DetectorEvent(DetectorEventType.addObject, data: group));

    _checkType(obj);
    String key = group;
    Expando? expando = _watchGroup[key];
    expando ??= Expando('LeakChecker$key');
    expando[obj] = true;
    _watchGroup[key] = expando;

  }

  static _checkType(object) {
    if ((object == null) ||
        (object is bool) ||
        (object is num) ||
        (object is String) ||
        (object is Pointer) ||
        (object is Struct)) {
      throw ArgumentError.value(object,
          "Expando 不允许使用 strings, numbers, booleans, null, Pointers, Structs 或 Unions.");
    }
  }
}

///检测器内部事件
class DetectorEvent {
  final DetectorEventType type;
  final dynamic data;

  @override
  String toString() {
    return '$type, $data';
  }

  DetectorEvent(this.type, {this.data});
}

enum DetectorEventType {
  addObject, //添加对象
  check,
  startGC,
  endGc,
  startAnalyze,
  endAnalyze,
}
