import 'package:orvibo_kit/kit/apm/launch/route_observer.dart';
import 'package:flutter/material.dart';

import 'apm/leak_detector/leak_navigator_observer.dart';

/// OrviboKit对外的路由观察者
/// 向内转发各个路由事件
class OrviboKitNavigatorObserver extends NavigatorObserver {
  final List<NavigatorObserver> _observers = [LaunchObserver(),LeakNavigatorObserver()];


  @override
  void didPush(Route route, Route? previousRoute) {
    super.didPush(route, previousRoute);
    assert((){
      _observers.forEach((element) {
        element.didPush(route, previousRoute);
      });
      return true;
    }());
  }

  @override
  void didPop(Route route, Route? previousRoute) {
    super.didPop(route, previousRoute);
    assert((){
      _observers.forEach((element) {
        element.didPop(route, previousRoute);
      });
      return true;
    }());
  }

  @override
  void didRemove(Route route, Route? previousRoute) {
    super.didRemove(route, previousRoute);
    assert((){
      _observers.forEach((element) {
        element.didRemove(route, previousRoute);
      });
      return true;
    }());
  }

  @override
  void didReplace({Route? newRoute, Route? oldRoute}) {
    super.didReplace(newRoute: newRoute, oldRoute: oldRoute);
    assert((){
      _observers.forEach((element) {
        element.didReplace(newRoute: newRoute, oldRoute: oldRoute);
      });
      return true;
    }());
  }

  @override
  void didStartUserGesture(Route route, Route? previousRoute) {
    super.didStartUserGesture(route, previousRoute);
    assert((){
      _observers.forEach((element) {
        element.didStartUserGesture(route, previousRoute);
      });
      return true;
    }());
  }

  @override
  void didStopUserGesture() {
    super.didStopUserGesture();
    assert((){
      _observers.forEach((element) {
        element.didStopUserGesture();
      });
      return true;
    }());

  }
}
