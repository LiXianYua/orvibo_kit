import 'package:orvibo_kit/kit/apm/leak_detector/leak_navigator_observer.dart';
import 'package:orvibo_kit/kit/observer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:orvibo_kit/orvibo_kit.dart';

final GlobalKey<OverlayState> orviboKitOverlayKey = GlobalKey<OverlayState>();

abstract class IOrviboKitApp extends Widget {
  Widget get origin;
}

// 谷歌提供的DevTool会判断入口widget是否在主工程内申明(runApp(MyApp())，MyApp必须在主工程内定义，估计是根据source file来判断的)，
// 如果在package内去申明这个入口widget，则在Flutter Inspector上的左边树会被折叠，影响开发使用。故这里要求在main文件内使用OrviboKitApp(MyApp())的形式来初始化入口
class OrviboKitApp extends StatefulWidget implements IOrviboKitApp {
  OrviboKitApp(Widget widget,{String? appVersion})
      : _origin = _OrviboKitWrapper(widget),
        super(key: rootKey){
    OrviboKitApp.version = appVersion;
  }

  // 放置orvibokit悬浮窗的容器
  static GlobalKey rootKey = GlobalKey();

  // 放置应用真实widget的容器
  static GlobalKey appKey = GlobalKey();

  static String? version;

  @override
  Widget get origin => _origin;
  final Widget _origin;

  @override
  State<StatefulWidget> createState() {
    return _OrviboKitAppState();
  }
}

class _OrviboKitWrapper extends StatelessWidget {
  _OrviboKitWrapper(this._origin) : super(key: OrviboKitApp.appKey);

  final Widget _origin;

  @override
  Widget build(BuildContext context) {
    if (_origin is StatelessWidget) {
      debugPrint(_origin.toStringShort());
      var widget = (_origin as StatelessWidget).build(context);
      debugPrint(widget.toStringShort());
      if (widget is MaterialApp) {
        final navigatorObservers = widget.navigatorObservers;
        if (navigatorObservers != null) {
          ensureOrviboKitObserver(navigatorObservers);
          return widget;
        }
      }
      if (widget is CupertinoApp) {
        final navigatorObservers = widget.navigatorObservers;
        if (navigatorObservers != null) {
          ensureOrviboKitObserver(navigatorObservers);
          return widget;
        }
      }
    }
    return _origin;
  }

  void ensureOrviboKitObserver(List<NavigatorObserver> navigatorObservers) {
    if (!navigatorObservers
        .any((element) => element is OrviboKitNavigatorObserver)) {
      navigatorObservers.add(OrviboKitNavigatorObserver());
    }
  }
}

class _OrviboKitAppState extends State<OrviboKitApp> {
  final List<OverlayEntry> entries = <OverlayEntry>[];
  final List<Locale> supportedLocales = const <Locale>[Locale('en', 'US')];

  @override
  void initState() {
    super.initState();
    entries.clear();
    entries.add(OverlayEntry(builder: (BuildContext context) {
      return widget.origin;
    }));
  }

  Iterable<LocalizationsDelegate<dynamic>> get _localizationsDelegates sync* {
    yield DefaultMaterialLocalizations.delegate;
    yield DefaultCupertinoLocalizations.delegate;
    yield DefaultWidgetsLocalizations.delegate;
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Directionality(
      textDirection: TextDirection.ltr,
      child: Stack(
        children: <Widget>[
          widget.origin,
          _MediaQueryFromWindow(
            child: Localizations(
              locale: supportedLocales.first,
              delegates: _localizationsDelegates.toList(),
              child: ScaffoldMessenger(
                child: Overlay(
                  key: orviboKitOverlayKey,
                ),
              ),
            ),
          )
        ],
      ),
    ),);
  }
}

class _MediaQueryFromWindow extends StatefulWidget {
  const _MediaQueryFromWindow({Key? key, required this.child})
      : super(key: key);

  final Widget child;

  @override
  _MediaQueryFromWindowsState createState() => _MediaQueryFromWindowsState();
}

class _MediaQueryFromWindowsState extends State<_MediaQueryFromWindow>
    with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addObserver(this);
  }

  // ACCESSIBILITY

  @override
  void didChangeAccessibilityFeatures() {
    setState(() {
      // The properties of window have changed. We use them in our build
      // function, so we need setState(), but we don't cache anything locally.
    });
  }

  // METRICS

  @override
  void didChangeMetrics() {
    setState(() {
      // The properties of window have changed. We use them in our build
      // function, so we need setState(), but we don't cache anything locally.
    });
  }

  @override
  void didChangeTextScaleFactor() {
    setState(() {
      // The textScaleFactor property of window has changed. We reference
      // window in our build function, so we need to call setState(), but
      // we don't need to cache anything locally.
    });
  }

  // RENDERING
  @override
  void didChangePlatformBrightness() {
    setState(() {
      // The platformBrightness property of window has changed. We reference
      // window in our build function, so we need to call setState(), but
      // we don't need to cache anything locally.
    });
  }

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQueryData.fromWindow(WidgetsBinding.instance!.window),
      child: widget.child,
    );
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);
    super.dispose();
  }
}
