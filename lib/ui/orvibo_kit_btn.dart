import 'package:orvibo_kit/orvibo_kit.dart';
import 'package:orvibo_kit/kit/apm/apm.dart';
import 'package:orvibo_kit/ui/orvibo_kit_app.dart';
import 'package:orvibo_kit/ui/resident_page.dart';
import 'package:flutter/material.dart';

// 入口btn
// ignore: must_be_immutable
class OrviboKitBtn extends StatefulWidget {
  OrviboKitBtn() : super(key: orviboKitBtnKey);

  static GlobalKey<OrviboKitBtnState> orviboKitBtnKey = GlobalKey<OrviboKitBtnState>();
  OverlayEntry? overlayEntry;

  @override
  OrviboKitBtnState createState() => OrviboKitBtnState();

  void addToOverlay() {
    assert(overlayEntry == null);
    overlayEntry = OverlayEntry(builder: (BuildContext context) {
      return this;
    });
    final rootOverlay = orviboKitOverlayKey.currentState;
    assert(rootOverlay != null);
    rootOverlay?.insert(overlayEntry!);
    ApmKitManager.instance.startUp();
  }
}

class OrviboKitBtnState extends State<OrviboKitBtn> {
  OrviboKitBtnState();

  Offset? offsetA; //按钮的初始位置
  late final OverlayEntry owner;
  OverlayEntry? debugPage;
  bool showDebugPage = false;

  @override
  void initState() {
    owner = widget.overlayEntry!;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
        left: offsetA?.dx,
        top: offsetA?.dy,
        right: offsetA == null ? 20 : null,
        bottom: offsetA == null ? 120 : null,
        child: Draggable(
            feedback: Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(100),color: Colors.lightGreen[200]),
              child: ClipOval(child: TextButton(
                onPressed: openDebugPage,
                onLongPress: testError,
                child: const Text("O",style: TextStyle(color: Colors.white)),
              )),
            ),
            childWhenDragging: Container(),
            onDragEnd: (DraggableDetails detail) {
              final offset = detail.offset;
              setState(() {
                final size = MediaQuery.of(context).size;
                final width = size.width;
                final height = size.height;
                var x = offset.dx;
                var y = offset.dy;
                if (x < 0) {
                  x = 0;
                }
                if (x > width - 80) {
                  x = width - 80;
                }
                if (y < 0) {
                  y = 0;
                }
                if (y > height - 26) {
                  y = height - 26;
                }
                offsetA = Offset(x, y);
              });
            },
            onDraggableCanceled: (Velocity velocity, Offset offset) {},
          child: Container(
            width: 50,
            height: 50,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(100),color: Colors.lightGreen[200]),
            child: ClipOval(child: TextButton(
              onPressed: openDebugPage,
              onLongPress: testError,
              child: const Text("O",style: TextStyle(color: Colors.white),),
            )),
          ),
        ));
  }

  void openDebugPage() {
    debugPage ??= OverlayEntry(builder: (BuildContext context) {
      return ResidentPage();
    });
    if (showDebugPage) {
      closeDebugPage();
    } else {
      orviboKitOverlayKey.currentState?.insert(debugPage!, below: widget.overlayEntry);
      showDebugPage = true;
    }
  }

  void closeDebugPage() {
    if (showDebugPage && debugPage != null) {
      debugPage!.remove();
      showDebugPage = false;
    }
  }

  void testError(){
    throw Exception("Test Error");
  }
}
