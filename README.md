##使用说明

###1.添加依赖

```
orvibo_kit:
    git:
      url: https://gitlab.com/LiXianYua/orvibo_kit.git
      ref: 0.0.2-dev # tag 对应版本
```

###2.修改App入口
将`runApp(App())`替换为`OrviboKit.runApp(app: OrviboKitApp(App(),appVersion: "1.0"))`，
`appVersion`为可选参数,可直接修改`OrviboKitApp.version`变量。

###3.添加navigatorObservers
在navigatorObservers中添加`OrviboKitNavigatorObserver`，未添加无法使用路由跳转耗时统计和对象泄漏检测功能

###4.禁用dds
修改main.dart的Configurations,在Additional run args中添加`--disable-dds`